package net.coronam.docker;

import com.google.gson.Gson;
import net.coronam.docker.entities.DockerManifest;
import net.coronam.docker.entities.DockerTags;
import net.coronam.docker.entities.DockerToken;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.List;

public class DockerRegistry {

    private static final String DOCKER_TOKEN_URL = "https://auth.docker.io/token?service=registry.docker.io&scope=repository:%s:pull";
    private static final String DOCKER_REGISTRY_URL = "https://registry-1.docker.io";

    private final OkHttpClient client = new OkHttpClient();
    private final Gson gson = new Gson();

    /**
     * @param repository repository name to get a token for (e.g. library/python, or opensuse/amd64)
     * @return a DockerToken entity, with the "Bearer" token and an expiry time
     * @throws IOException if anything goes wrong during the HTTP request
     * @todo use a cache (e.g. guava cachemaker) with lifetime < expiry to save on request AND skip complicated "if token has expired since our last request with it" logic
     */
    public DockerToken getToken(final String repository) throws IOException {
        final Request request = new Request.Builder()
                .url(String.format(DOCKER_TOKEN_URL, repository))
                .build();

        final Response response = client.newCall(request).execute();

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        return gson.fromJson(response.body().charStream(), DockerToken.class);
    }

    /**
     *
     * @param repository repository name to list tags for (e.g. library/python, or opensuse/amd64)
     * @return a list of tags for this repository
     * @throws IOException if anything goes wrong during the HTTP request
     */
    public List<String> listTags(final String repository) throws IOException {
        final String token = getToken(repository).getToken();

        final Response response = client.newCall(new Request.Builder()
                .url(DOCKER_REGISTRY_URL + String.format("/v2/%s/tags/list", repository))
                .addHeader("Authorization", String.format("Bearer %s", token))
                .build()
        ).execute();

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        return gson.fromJson(response.body().charStream(), DockerTags.class).getTags();
    }

    /**
     *
     * @param repository repository name to get manifest for (e.g. library/python, or opensuse/amd64)
     * @param tag repository tag to get manifest for (e.g. latest, 3.5, or tumbleweed)
     * @return manifest instance for repository+tag combination
     * @throws IOException if anything goes wrong during the HTTP request
     */
    public DockerManifest getManifest(final String repository, final String tag) throws IOException {
        final String token = getToken(repository).getToken();

        final Response response = client.newCall(new Request.Builder()
                .url(DOCKER_REGISTRY_URL + String.format("/v2/%s/manifests/%s", repository, tag))
                .addHeader("Authorization", String.format("Bearer %s", token))
                .build()
        ).execute();

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        return gson.fromJson(response.body().charStream(), DockerManifest.class);
    }
}
