package net.coronam.docker.entities;


public class DockerToken {
    private String token;
    private Integer expires_in;
    private String issued_at;

    public DockerToken(String token, Integer expires_in, String issued_at) {
        this.token = token;
        this.expires_in = expires_in;
        this.issued_at = issued_at;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
    }

    public String getIssued_at() {
        return issued_at;
    }

    public void setIssued_at(String issued_at) {
        this.issued_at = issued_at;
    }

    @Override
    public String toString() {
        return "DockerToken{" +
                "token='" + token + '\'' +
                ", expires_in=" + expires_in +
                ", issued_at='" + issued_at + '\'' +
                '}';
    }
}
