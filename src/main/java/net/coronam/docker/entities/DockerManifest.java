package net.coronam.docker.entities;


import com.google.gson.JsonObject;

import java.util.List;

public class DockerManifest {
    private String name;
    private String tag;

    private Integer schemaVersion;

    private String architecture;

    private List<JsonObject> fsLayers;

    private List<JsonObject> signatures;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(Integer schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    public List<JsonObject> getFsLayers() {
        return fsLayers;
    }

    public void setFsLayers(List<JsonObject> fsLayers) {
        this.fsLayers = fsLayers;
    }

    public List<JsonObject> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<JsonObject> signatures) {
        this.signatures = signatures;
    }

    @Override
    public String toString() {
        return "DockerManifest{" +
                "name='" + name + '\'' +
                ", tag='" + tag + '\'' +
                ", schemaVersion=" + schemaVersion +
                ", architecture='" + architecture + '\'' +
                ", fsLayers=" + fsLayers +
                ", signatures=" + signatures +
                '}';
    }
}