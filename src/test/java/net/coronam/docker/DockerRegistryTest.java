package net.coronam.docker;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.JsonNode;
import net.coronam.docker.entities.DockerManifest;
import net.coronam.docker.entities.DockerToken;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class DockerRegistryTest {

    @Test
    public void getToken() throws IOException {
        // GIVEN
        final String testRepository = "library/opensuse";

        // WHEN
        final DockerToken token = new DockerRegistry().getToken(testRepository);
        final JWT jwt = JWT.decode(token.getToken());

        // THEN
        assertThat(jwt.getIssuer()).isEqualTo("auth.docker.io");

        final List<JsonNode> accessClaims = jwt.getClaim("access").asList(JsonNode.class);
        assertThat(accessClaims).hasSize(1);

        final JsonNode access = accessClaims.get(0);
        assertThat(access.get("type").asText()).isEqualTo("repository");
        assertThat(access.get("name").asText()).isEqualTo(testRepository);
    }

    @Test
    public void listTags() throws IOException {
        // GIVEN
        final String testRepository = "library/opensuse";
        final String testTag = "tumbleweed";


        // WHEN
        final List<String> tags = new DockerRegistry().listTags(testRepository);

        // THEN
        assertThat(tags).isNotEmpty();
        assertThat(tags).contains(testTag);
    }

    @Test
    public void getManifest() throws IOException {
        // GIVEN
        final String testRepository = "library/opensuse";
        final String testTag = "tumbleweed";

        // WHEN
        final DockerManifest manifest = new DockerRegistry().getManifest(testRepository, testTag);

        // THEN
        assertThat(manifest.getName()).isEqualTo(testRepository);
        assertThat(manifest.getTag()).isEqualTo(testTag);
    }

    @Test(expected = IOException.class)
    public void getManifest_missingTagThrows() throws IOException {
        // GIVEN
        final String testRepository = "library/opensuse";
        final String testTag = "xenial";

        // WHEN
        new DockerRegistry().getManifest(testRepository, testTag);

        // THEN
        fail("Should have thrown!");
    }

}
