# docker-registry-client

Java 8 Library for accessing a docker registry service. Only supports `v2` APIs.

## Development

* Running the testsuite: `mvn test`


## Issues

* The "official" registry run by Docker Inc. does not allow certain bandwidth or processing heavy calls, e.g. "listing all repositories in the catalog" ([source](http://stackoverflow.com/a/37518393))


## TODO

In a vague order of importance/usefulness, this should be next:

* **cache and reuse auth tokens intelligently by their scope**.
  
    Using a simple "caching slightly shorter than the tokens expiration time" approach here allows us to not implement smart "do i have a token -> is this token still valid" logic. We still have to face tokens being invalidated on the backend though.

* **Design a useful set of exceptions instead of throwing `IOException` upwards**
 
* **allow registry and auth endpoints to be configured**

    It's very probable that auth works different for third party registries, so that should probably become it's own plugable strategy at that point.

* **add findbugs/pmd/checkstyle**

* **add an automated code formatter to avoid future style discussions**

* **write tests that mock out the actual registry service**
 
    Without accidentally implementing a registry while doing so.
  

